# -*- coding: utf-8 -*-

__author__ = 'Helene Flakke & Stian Teien'

import textwrap
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, TextBox
import matplotlib.image as mpimg
import numpy as np
from biosim.animals import *
import examples.population_generator
import re



class Visualize:
    def __init__(self, map_geografi, coords, sim):
        """
        The visualize class makes a figure with plots showing the standstill map in use,
        the demographic development of the island, the herbivore population head count and
        spreading, and the carnivore head count and spreading.

        :param map_geografi: the map as a string
        :param coords: the coordinates as object
        :param sim: takes in the simulation, for updating parameters mid simulation
        """

        self.fig = plt.figure(figsize=(12, 8))

        self.ax1 = plt.axes([0.07, 0.65, 0.32, 0.32])    # Main map
        self.ax2 = plt.axes([0.55, 0.65, 0.32, 0.32])    # Population curves
        self.ax3 = plt.axes([0.07, 0.25, 0.41, 0.32])    # Herbivore population
        self.ax4 = plt.axes([0.55, 0.25, 0.41, 0.32])    # Carnivore population

        self.ax5 = plt.axes([0.09, 0.13, 0.25, 0.015])   # Herbivore parameter
        self.ax6 = plt.axes([0.09, 0.11, 0.25, 0.015])   # Herbivore parameter
        self.ax7 = plt.axes([0.59, 0.13, 0.25, 0.015])   # Carnivore parameter
        self.ax8 = plt.axes([0.59, 0.11, 0.25, 0.015])   # Carnivore parameter

        self.ax9 = plt.axes([0.91, 0.04, 0.07, 0.027])   # Pause button
        self.ax10 = plt.axes([0.83, 0.04, 0.07, 0.027])  # Interrupt button
        self.ax11 = plt.axes([0.75, 0.04, 0.07, 0.027])  # Start button

        self.ax12 = plt.axes([0.15, 0.07, 0.07, 0.025])  # Input for amount of years
        self.ax13 = plt.axes([0.15, 0.04, 0.07, 0.025])  # Amount of hearbivores
        self.ax14 = plt.axes([0.15, 0.01, 0.07, 0.025])  # Amount of carnivores

        self.ax15 = plt.axes([0.88, 0.95, 0.04, 0.025])  # Numbers of herbivores alive, top right
        self.ax16 = plt.axes([0.88, 0.92, 0.04, 0.025])  # Numbers of carnivores alive, top right

        self.ax17 = plt.axes([0.39, 0.04, 0.09, 0.025])  # Insert coords for herbivores
        self.ax18 = plt.axes([0.39, 0.01, 0.09, 0.025])  # Insert coords for carnivores

        self.ax19 = plt.axes([0.84, 0.12, 0.20, 0.05])   # Image for loading simualtion
        self.ax20 = plt.axes([0.88, 0.87, 0.04, 0.025])  # Year counter
        self.ax21 = plt.axes([0.75, 0.01, 0.07, 0.027])  # Make movie button

        self.ax22 = plt.axes([0.59, 0.04, 0.04, 0.025])       # vis_year, years per simualtion
        self.ax23 = plt.axes([0.59, 0.01, 0.04, 0.025])       # img_year, years per image
        self.ax24 = plt.axes([0.0125, 0.19, 0.975, 0.00025])  # Divider

        self.map_geografi = map_geografi
        self.coords = coords
        self.sim = sim
        self.interrupt_boolean = False

    def setup_graphics(self, carn_colorbar_limit, herb_colorbar_limit, y_axis_limit):
        """
        Starts setup for all graphics-elements in figure
        """
        self.make_static_map()
        self.setup_line_plot(y_axis_limit)
        self.setup_herb_population(herb_colorbar_limit)
        self.setup_carn_population(carn_colorbar_limit)
        self.setup_sliders()
        self.setup_buttons_and_textbox()

    def make_static_map(self):
        """
        Makes island layout with colors and takes in ticks for every location.
        Code made by Hans E. Plesser
        """
        self.ax1.set_title("Island layout")
        rgb_value = {'O': (0.0, 0.5, 1.0),  # blue
                     'M': (0.5, 0.5, 0.5),  # grey
                     'J': (0.0, 0.6, 0.0),  # dark green
                     'S': (0.5, 1.0, 0.5),  # light green
                     'D': (1.0, 1.0, 0.5)}  # light yellow

        kart_rgb = [[rgb_value[column] for column in row] for row in
                    textwrap.dedent(self.map_geografi).splitlines()]

        self.ax1.imshow(kart_rgb)
        self.ax1.set_xticks(range(len(kart_rgb[0])))
        self.ax1.set_xticklabels(range(1, 1 + len(kart_rgb[0])))
        self.ax1.set_yticks(range(len(kart_rgb)))
        self.ax1.set_yticklabels(range(1, 1 + len(kart_rgb)))

    def setup_line_plot(self, y_axis_limit):
        """
        Setup plot for graph of population for herbivores and carnivores.
        """
        self.ax2.set_title("Population")
        self.ax2.set_xlim(0, 5)
        self.ax2.set_ylim(0, y_axis_limit)
        self.n = 0
        self.line_herbivore = self.ax2.plot(np.arange(0, 100),
                                            np.full(100, np.nan), 'b-')[0]

        self.line_carnivore = self.ax2.plot(np.arange(100),
                                  np.full(100, np.nan), 'r-')[0]

        self.ax2.legend((self.line_herbivore, self.line_carnivore),
                        ('Herbivores', 'Carnivores'), loc=2)

    def update_line(self, list_herb, list_carn, num_years):
        """
        Updates graph of herbivores and carnivores. Changes data and updates plot.
        Expand y- and x-axis when graph reach the axes limits.

        :param list_herb: list of herbivore population on whole island
        :param list_carn: list of carnivore population on whole island
        """

        # Change x-axes
        if (self.ax2.get_xlim()[1]) <= len(list_herb)*num_years:
            self.ax2.set_xlim(0, len(list_herb)*num_years * 1.3)

        # Change y-axes
        if (self.ax2.get_ylim()[0]) <= max(list_herb):
            self.ax2.set_ylim(0, max(list_herb) * 1.2)

        ydata_herb = self.line_herbivore.get_ydata()
        ydata_carn = self.line_carnivore.get_ydata()
        xdata = self.line_herbivore.get_xdata()
        self.n += 1

        # Make line bigger if necessary
        if self.n >= ydata_herb.size:
            ydata_herb = (np.concatenate((ydata_herb, np.full(100, np.nan)), axis=None))
            ydata_carn = (np.concatenate((ydata_carn, np.full(100, np.nan)), axis=None))
            xdata = np.concatenate((xdata, np.arange(xdata.size, xdata.size+100)), axis=None)

            self.line_herbivore.set_ydata(ydata_herb)
            self.line_herbivore.set_xdata(xdata)

            self.line_carnivore.set_ydata(ydata_carn)
            self.line_carnivore.set_xdata(xdata)

        xdata[self.n] = self.n * num_years
        ydata_herb[self.n] = list_herb[-1]
        ydata_carn[self.n] = list_carn[-1]

        self.line_herbivore.set_xdata(xdata)
        self.line_carnivore.set_xdata(xdata)
        self.line_herbivore.set_ydata(ydata_herb)
        self.line_carnivore.set_ydata(ydata_carn)

    def setup_herb_population(self, colorbar_limit):
        """
        Setup map for herbivore density on island with color bar.
        """

        self.ax3.set_title("Herbivore population")
        kart_pop = [[len(cell.herbivore_in_cell) for cell in rows] for rows in self.coords]

        self.im_herb = self.ax3.imshow(kart_pop)
        self.ax3.set_xticks(range(len(kart_pop[0])))
        self.ax3.set_xticklabels(range(1, 1 + len(kart_pop[0])))
        self.ax3.set_yticks(range(len(kart_pop)))
        self.ax3.set_yticklabels(range(1, 1 + len(kart_pop)))

        self.im_herb.set_clim(vmin=0, vmax=colorbar_limit)
        plt.colorbar(self.im_herb, ax=self.ax3)

    def update_herb_population(self):
        """
        Updates data of herbivore density on island. Counts herbivores in every cell.
        """
        kart_pop = [[len(cell.herbivore_in_cell) for cell in rows] for rows in self.coords]
        self.im_herb.set_data(kart_pop)

    def setup_carn_population(self, colorbar_limit):
        """
        Setup map for carnivore density on island with color bar.
        """

        self.ax4.set_title("Carnivore population")
        kart_pop = [[len(cell.carnivore_in_cell) for cell in rows] for rows in self.coords]

        self.im_carn = self.ax4.imshow(kart_pop)
        self.ax4.set_xticks(range(len(kart_pop[0])))
        self.ax4.set_xticklabels(range(1, 1 + len(kart_pop[0])))
        self.ax4.set_yticks(range(len(kart_pop)))
        self.ax4.set_yticklabels(range(1, 1 + len(kart_pop)))

        self.im_carn.set_clim(vmin=0, vmax=colorbar_limit)
        plt.colorbar(self.im_carn, ax=self.ax4)

    def update_carn_population(self):
        """
        Updates data of carnivore density on island. Counts carnivores in every cell.
        """
        kart_pop = [[len(cell.carnivore_in_cell) for cell in rows] for rows in self.coords]
        self.im_carn.set_data(kart_pop)

    def setup_sliders(self):
        """
        Setup sliders for changing parameters for herbivores and carnivores
        """
        self.ax5.set_title("Herbivore parameters")
        self.herb_omega = Slider(ax=self.ax5,
                                 label='Omega',
                                 valmin=0,
                                 valmax=1,
                                 valinit=Herbivore.parameters['omega'])

        self.herb_gamma = Slider(ax=self.ax6,
                                 label='Gamma',
                                 valmin=0,
                                 valmax=1,
                                 valinit=Herbivore.parameters['gamma'])

        self.ax7.set_title("Carnivore parameters")
        self.carn_omega = Slider(ax=self.ax7,
                                 label='Omega',
                                 valmin=0,
                                 valmax=1,
                                 valinit=Carnivore.parameters['omega'])

        self.carn_gamma = Slider(ax=self.ax8,
                                 label='Gamma',
                                 valmin=0,
                                 valmax=1,
                                 valinit=Carnivore.parameters['gamma'])

        self.herb_omega.on_changed(self.update_sliders)
        self.herb_gamma.on_changed(self.update_sliders)
        self.carn_omega.on_changed(self.update_sliders)
        self.carn_gamma.on_changed(self.update_sliders)

    def update_sliders(self, val):
        """
        Updates and change parameters for Herbivores and Carnivores classes
        """
        Herbivore.parameters['omega'] = self.herb_omega.val
        Herbivore.parameters['gamma'] = self.herb_gamma.val
        Carnivore.parameters['omega'] = self.carn_omega.val
        Carnivore.parameters['gamma'] = self.carn_gamma.val

    def setup_buttons_and_textbox(self):
        """
        Setup buttons and text boxes in figure.
        """
        self.pause_btn = Button(self.ax9, "Pause")
        self.pause_btn.on_clicked(self.pause)
        self.paused = False

        self.interrupt_btn = Button(self.ax10, "Interrupt")
        self.interrupt_btn.on_clicked(self.interrupt)

        self.start_btn = Button(self.ax11, "Start")
        self.start_btn.on_clicked(self.start_sim)

        self.num_year = TextBox(self.ax12, "Number of years ", initial="100")
        self.num_herb = TextBox(self.ax13, "Number of Herbivores ", initial="30")
        self.num_carn = TextBox(self.ax14, "Number of Carnivores ", initial="5")
        self.herb_coord = TextBox(self.ax17, "Coordinates for Herbivores ", initial="(2, 2), (2, 17)")
        self.carn_coord = TextBox(self.ax18, "Coordinates for Carnivores ", initial="(2, 2), (2, 17)")

        self.alive_data_herb = "0"
        self.alive_data_carn = "0"
        self.herb_alive = self.ax15.text(0, 0, "Herbivores: "+self.alive_data_herb)
        self.carn_alive = self.ax16.text(0, 0, "Carnivores: "+self.alive_data_carn)
        self.ax15.axis('off')
        self.ax16.axis('off')

        self.year_counter = self.ax20.text(0, 0, "Year: "+"0")
        self.ax20.axis('off')

        img = mpimg.imread('../img/loading.png')
        self.ax19.imshow(img)
        self.ax19.axis('off')
        self.ax19.set_visible(False)

        self.make_movie_btn = Button(self.ax21, "Make movie")
        self.make_movie_btn.on_clicked(self.make_movie)

        self.vis_year_txt = TextBox(self.ax22, "Years per update ", initial="1")
        self.img_year_txt = TextBox(self.ax23, "Years per image ", initial="None")

        self.ax24.get_xaxis().set_visible(False)
        self.ax24.get_yaxis().set_visible(False)


    def pause(self, event):
        """
        Pauses the simulation with a while loop. Changes button label to resume.
        """
        if not self.paused:
            self.paused =  True
            self.pause_btn.label.set_text("Resume")
            while self.paused:
                plt.pause(1)
        else:
            self.paused = False
            self.pause_btn.label.set_text("Pause")

    def interrupt(self, event):
        """
        Changes an attribute to be true and breaks the simulation loop
        """
        self.interrupt_boolean = True

    def start_sim(self, event):
        """
        Start simulation and add animals.
        """
        if self.img_year_txt.text is not "None":
            img_year_txt = eval(self.img_year_txt.text)
        else:
            img_year_txt = None

        herb_coords = (re.findall('\d+', self.herb_coord.text))
        carn_coords = (re.findall('\d+', self.carn_coord.text))
        herb_coords, carn_coords = self.make_tuple_coords(herb_coords, carn_coords)

        added_population = examples.population_generator.Population(
            n_herbivores=eval(self.num_herb.text), coord_herb=herb_coords,
            n_carnivores=eval(self.num_carn.text), coord_carn=carn_coords).get_animals()

        self.sim.add_population(added_population)
        self.sim.simulate(eval(self.num_year.text),
                          vis_years=eval(self.vis_year_txt.text), img_years=img_year_txt)
        self.ax19.set_visible(False)

    @staticmethod
    def make_tuple_coords(herb_coords, carn_coords):
        """
        Makes tuple out of string. Input collected from figure.

        :param herb_coords: coordinates where new herbivores will be added in the map
        :param carn_coords: coordinates where new carnivores will be added in the map
        :return: coordinates as a list with tuples
        """
        herb_new_coords = []
        carn_new_coords = []
        for i in range(0, len(herb_coords), 2):
            temp = [0, 0]
            temp[0] = int(herb_coords[i])
            temp[1] = int(herb_coords[i + 1])
            herb_new_coords.append(tuple(temp))

        for i in range(0, len(carn_coords), 2):
            temp = [0, 0]
            temp[0] = int(carn_coords[i])
            temp[1] = int(carn_coords[i + 1])
            carn_new_coords.append(tuple(temp))

        return herb_new_coords, carn_new_coords

    def update_animals_alive(self, n_animals, n_year):
        """
        Updates animal and year counter.

        :param n_animals: list of herbivores and carnivores
        :param n_year: integer describing number of years simulated
        """
        self.alive_data_herb = n_animals['Herbivore']
        self.alive_data_carn = n_animals['Carnivore']

        self.herb_alive.set_text("Herbivores: "+str(self.alive_data_herb))
        self.carn_alive.set_text("Carnivores: "+str(self.alive_data_carn))
        self.year_counter.set_text("Year: " + str(n_year))

    def make_movie(self, event):
        """
        Makes movie from simulation. Uses images from ../img/ - folder in project.
        """
        self.sim.make_movie()