# -*- coding: utf-8 -*-

__author__ = 'Helene Flakke & Stian Teien'


import random
import numpy as np
import math


class Animal:
    """animals.py contains the classes necessary to create the two different types of animals living at
    Rossumøya: herbivores and carnivores. The animal classes sets up various methods for the animals to give birth,
    get older, lose or gain weight, and die from old age. Their respective classes will implement their way of eating,
    whether it is plant food or pray. The simulation of the years passing by and the development of the island will
    be found in the simulation.py file with visualisation given by visualization.py.
    The parameters are used to calculate the state of the animals

    :param w_birth: birth weight mean
    :param sigma_birth: birth weight standard deviation
    :param beta: fodder weight increase factor
    :param eta: weight loss factor at end of year

    :param a_half: age turning point
    :param phi_age: age factor
    :param w_half: weight turning point
    :param phi_weight: weight factor

    :param mu: migration factor
    :param gamma: birth-factor
    :param zeta: birth-factor
    :param xi: lost birth weight multiplier
    :param omega: death probability factor
    :param F: amount of food wanted
    :param DeltaPhiMax: fitness difference for carnivore eating
    """
    def __init__(self, age=3, weight=None):
        if age < 0 or weight <= 0:
            raise ValueError
        self.weight = weight
        self.age = age
        self.fitness = self._calc_fitness()
        self.can_migrate = True

    def _calc_fitness(self):
        """
        Method for calculating the animals fitness,
        depends on their age, weight, and other given parameters

        :return: fitness-value
        """
        if self.weight > 0:
            q_pluss = 1/(1+math.exp(+self.parameters['phi_age']*(self.age - self.parameters['a_half'])))
            q_minus = 1/(1+math.exp(-self.parameters['phi_weight']*(self.weight - self.parameters['w_half'])))
            return q_pluss * q_minus
        else:
            return 0

    def age_increase(self):
        """
        Method for increasing the age of the animals by 1, and will
        update the animals fitness value as it does so.

        """
        self.age += 1
        self.fitness = self._calc_fitness()

    def weight_loss(self):
        """
        Method for calculating the yearly weight loss suffered
        by all animals as the year come to an end

        """
        self._weight_changes(0, self.weight, self.parameters['eta'])

    def give_birth(self, N):
        """
        Depending on their fitness, weight and the presence of others of the same species,
        the animal have a chance to produce an offspring. This method tests if conditions are met,
        rolls for probability, creates the offspring.

        :param N: amount of same species in same cell
        :return: an instanced object of the species
        """

        if self.weight > self.parameters['zeta']*(self.parameters['w_birth'] + self.parameters['sigma_birth']):
            if min(1, self.parameters['gamma'] * self.fitness * (N - 1)) >= random.random():

                # mother loses baby weight
                new_born_weight = np.random.normal(self.parameters['w_birth'], self.parameters['sigma_birth'])
                if new_born_weight > 0:  # make sure the weight of newborn is positive
                    if new_born_weight*self.parameters['xi'] < self.weight:  # guarantee the mother have enough weight
                        self._weight_changes(0, new_born_weight, self.parameters['xi'])  # mothr loses the weight
                        return self.__class__(age=0, weight=new_born_weight)  # return a new animal

    def dies_naturally(self):
        """
        The lower the fitness of the animal, the higher the chance of it dying of old age or starve
        to death, the probability of this occurring scaling with the parameter omega.

        :return: True if the animal dies this year
        """
        if self.fitness == 0:
            return True

        if self.parameters['omega'] * (1 - self.fitness) >= random.random():
            return True

    def migrate(self):
        """
        Migrating happen if the animals win the game of probability against random, and it is
        actually possible to move to its "desired" location. This is tested in the map.

        :return: a tuple affecting the coordinates of the animal in map.py
        """
        if self.fitness*self.parameters['mu'] >= random.random():
            random_retning = ((1, 0), (0, 1), (-1, 0), (0, -1))
            return random.choice(random_retning)

    def _weight_changes(self, increase, decrease, event_constant):
        """
        Animal changes weight depending on the event and the corresponding constant,
        no matter the event, and method updates the weight and fitness
        """
        self.weight += increase*event_constant - decrease*event_constant
        self.fitness = self._calc_fitness()


class Herbivore(Animal):
    """
    Are a grass eating animal class, feeds off of savanna and jungle. Inherits most of its methods
    from the Animal superclass.
    """

    parameters = {'w_birth': 8.0,
                  'sigma_birth': 1.5,
                  'beta': 0.9,
                  'eta': 0.05,
                  'a_half': 40.0,
                  'phi_age': 0.2,
                  'w_half': 10.0,
                  'phi_weight': 0.1,
                  'mu': 0.25,
                  'gamma': 0.2,
                  'zeta': 3.5,
                  'xi': 1.2,
                  'omega': 0.4,
                  'F': 10.00}

    def __init__(self, age=3, weight=None):
        if weight is None:   # to be able to generate a random weight if None is provided
            weight = np.random.normal(20, 3)

        super().__init__(age, weight)

    def eat(self, food):
        """The herbivore wants to eat F amounts of food, and if less is
        available will eat the remaining food

        :param food: the food available in the cell before eating
        :return food: the food available in the cell after eating
        """

        if food > 0:
            if food >= self.parameters['F']:
                eats = self.parameters['F']
            else:
                eats = food

            self._weight_changes(eats, 0, self.parameters['beta'])
            return food - eats
        else:
            return food

    @classmethod
    def set_parameters(cls, endres):
        """update the parameters according to input parameters

        :param endres: a dictionary of parameters to change for the herbivore
        """
        cls.parameters.update(endres)


class Carnivore(Animal):
    """Prays on herbivores. Inherits most of its methods from the Animal superclass."""

    parameters = {'w_birth': 6,
                  'sigma_birth': 1,
                  'beta': 0.75,
                  'eta': 0.125,
                  'a_half': 60.0,
                  'phi_age': 0.4,
                  'w_half': 4.0,
                  'phi_weight': 0.4,
                  'mu': 0.4,
                  'gamma': 0.8,
                  'zeta': 3.5,
                  'xi': 1.1,
                  'omega': 0.9,
                  'F': 50.00,
                  'DeltaPhiMax': 10.0}

    def __init__(self, age=3, weight=None):
        if weight is None:  # to be able to generate a random weight if None is provided
            weight = np.random.normal(self.w_birth, self.sigma_birth)
        self.fodder_consumed = 0

        super().__init__(age, weight)

    def eat(self, pray_fitness, pray_weight):
        """carnivores will continue to try eating herbivores until there are no more herbivores
        to try to eat or it has had its fill. fittest carnivores starts on weakest herbivores

        :param pray_fitness: the fitness of the herbivore the carnivore is trying to eat
        :param pray_weight: the weight of the herbivore in case the carnivore gets to eat it
        :return: True if pray gets eaten so we know to delete the herbivore
        """

        if self.fitness > pray_fitness:
            if 0 < self.fitness-pray_fitness < self.parameters['DeltaPhiMax']:
                if ((self.fitness-pray_fitness)/self.parameters['DeltaPhiMax']) >= random.random():
                    if pray_weight > self.parameters['F']-self.fodder_consumed:
                        pray_weight = self.parameters['F']-self.fodder_consumed
                        # changes the weight of the pray animal so
                        # the carnivore only eats as much as F will allow

                    self._weight_changes(pray_weight, 0, self.parameters['beta'])
                    self.fodder_consumed += pray_weight
                    return True

            else:
                if pray_weight >= self.parameters['F'] - self.fodder_consumed:
                    pray_weight = self.parameters['F'] - self.fodder_consumed
                    # changes the weight of the pray animal so
                    # the carnivore only eats as much as F will allow

                self._weight_changes(pray_weight, 0, self.parameters['beta'])
                self.fodder_consumed += pray_weight
                return True

    @classmethod
    def set_parameters(cls, endres):
        """update the parameters according to input parameters

        :param endres: a dictionary over parameters to change for carnivores.
        """
        cls.parameters.update(endres)
