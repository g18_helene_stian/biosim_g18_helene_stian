# -*- coding: utf-8 -*-

__author__ = 'Helene Flakke & Stian Teien'


import textwrap
from biosim.landscape import *
from biosim.animals import *
import itertools


class Kart:
    def __init__(self, geografi):
        """The map file gathers the information about how the animals function and how the
        different landscape types work and place it all on a grid - and this is where the annual
        cycles are looped through and the values of the animals and landscapes are updated.
        The class creates an island with each cell as an object. Has a lot of hidden functionality
        to simplify the passing of a full year at the island

        :param geografi: the map is passed from a dictionary
        """

        geogr = textwrap.dedent(geografi)
        rows = geogr.split("\n")

        for i in range(len(rows[0])):
            if not(rows[0][i] == "O" and rows[-1][i] == "O"):
                raise ValueError("Island needs to be surrounded with water on all edges")

        for i in rows:
            if not(i[0] == "O" and i[-1] == "O"):
                raise ValueError("Island needs to be surrounded with water on all edges")

        for a, b in itertools.combinations(rows, 2):
            if not len(a) == len(b):
                raise ValueError("Island needs to have a rectangular shape")

        # Make nested list out of map-input with landscape types
        self.coordinates = [[give_type(i) for i in col] for col in rows]

    def insert_animals(self, animal_list):
        """
        Inserts animal in given cell based on input list. The locations of the animal in the input
        assumes a starting point of (1, 1).

        :param animal_list: dictionary of animals and their location
        """
        if animal_list:
            for locations in animal_list:
                xy_coords = locations['loc']  # xy_coords start at (1,1) from input
                if not self.coordinates[xy_coords[0] - 1][xy_coords[1] - 1].traversible:
                    raise ValueError("Animal can't be placed in an area not traversible")

                for animal in locations['pop']:
                    species = animal['species']
                    if species == 'Herbivore':
                        self.coordinates[xy_coords[0] - 1][xy_coords[1] - 1].\
                            herbivore_in_cell.append(Herbivore(animal['age'], animal['weight']))
                    elif species == "Carnivore":
                        self.coordinates[xy_coords[0] - 1][xy_coords[1] - 1].\
                            carnivore_in_cell.append(Carnivore(animal['age'], animal['weight']))
                    else:
                        raise ValueError("Animal type not recognised")

    def _annual_regrowth(self):
        """
        Regrowth of fodder in all cells depending on their landscape type.
        """

        for rows in self.coordinates:
            for cell in rows:
                if cell.__class__.__name__ == "Jungle" or cell.__class__.__name__ == "Savannah":
                    cell.new_year_new_food()

    def _annual_eating(self):
        """
        Annual eating for herbivores and carnivores, the herbivores eat first and their fitness
        are adjusted, then the carnivores eat. The carnivores will eat in order of fitness,
        attempting to eat the most unfit herbivores first.
        """

        # Herbivores eat
        for rows in self.coordinates:
            for cell in rows:

                if cell.herbivore_in_cell:

                    # Sort herbivores by fitness, highest first
                    cell.herbivore_in_cell.sort(key=lambda herbivore: herbivore.fitness, reverse=True)

                    # Herbivores eat based on fitness
                    for herbivore in cell.herbivore_in_cell:
                        cell.food_available = herbivore.eat(cell.food_available)

                    # Sort herbivores by fitness, lowest first
                    cell.herbivore_in_cell.sort(key=lambda herbivore: herbivore.fitness)

                if cell.carnivore_in_cell:

                    # Sort carnivores by fitness, highest first
                    cell.carnivore_in_cell.sort(key=lambda carnivore: carnivore.fitness, reverse=True)

                    # carnivores try to eat all herbivores, highest fitness try to eat first
                    for carnivore in cell.carnivore_in_cell:
                        herbivores_eaten = []

                        for herbivore in cell.herbivore_in_cell:
                            if carnivore.fodder_consumed < carnivore.parameters['F']:
                                if carnivore.eat(herbivore.fitness, herbivore.weight):
                                    herbivores_eaten.append(herbivore)
                            else:
                                break

                        carnivore.fodder_consumed = 0  # resets food "counter" eaten for that carnivore
                        cell.herbivore_in_cell = list(set(cell.herbivore_in_cell) - set(herbivores_eaten))

                        # Sorting herbivores anew, lowest first
                        cell.herbivore_in_cell.sort(key=lambda carnivore: carnivore.fitness)

    def _annual_give_birth(self):
        """
        Every year the animals in each cell might produce offspring, here these newborns are
        added to that cell

        """

        for rows in self.coordinates:
            for cell in rows:

                if cell.herbivore_in_cell:
                    baby_herbivores_list = []
                    for herbivore in cell.herbivore_in_cell:
                        new_born_herbivore = herbivore.give_birth(len(cell.herbivore_in_cell))
                        if new_born_herbivore:
                            baby_herbivores_list.append(new_born_herbivore)
                    cell.herbivore_in_cell.extend(baby_herbivores_list)

                if cell.carnivore_in_cell:
                    baby_carnivores_list = []
                    for carnivore in cell.carnivore_in_cell:
                        new_born_carnivore = carnivore.give_birth(len(cell.carnivore_in_cell))
                        if new_born_carnivore:
                            baby_carnivores_list.append(new_born_carnivore)
                    cell.carnivore_in_cell.extend(baby_carnivores_list)

    def _annual_migration(self):
        """
        Animals migrate with a certain probability.
        """

        for y, rows in enumerate(self.coordinates):
            for x, cell in enumerate(rows):

                # Migration for herbivores
                if cell.herbivore_in_cell:
                    herbivores_stays_in_cell = []
                    for herbivore in cell.herbivore_in_cell:
                        retning = herbivore.migrate()

                        if retning:
                            new_coords = (y + retning[0], x + retning[1])
                            if (herbivore.can_migrate) and retning and \
                                    self.coordinates[new_coords[0]][new_coords[1]].traversible:
                                herbivore.can_migrate = False
                                self.coordinates[new_coords[0]][new_coords[1]].herbivore_in_cell.append(herbivore)
                            else:
                                herbivores_stays_in_cell.append(herbivore)
                        else:
                            herbivores_stays_in_cell.append(herbivore)

                    cell.herbivore_in_cell = herbivores_stays_in_cell

                # Mirgration for carnivore
                if cell.carnivore_in_cell:
                    carnivores_stays_in_cell = []
                    for carnivore in cell.carnivore_in_cell:
                        retning = carnivore.migrate()

                        if retning:
                            new_coords = (y + retning[0], x + retning[1])
                            if (carnivore.can_migrate) and retning and \
                                    self.coordinates[new_coords[0]][new_coords[1]].traversible:

                                carnivore.can_migrate = False
                                self.coordinates[new_coords[0]][
                                    new_coords[1]].carnivore_in_cell.append(carnivore)
                            else:
                                carnivores_stays_in_cell.append(carnivore)
                        else:
                            carnivores_stays_in_cell.append(carnivore)

                    cell.carnivore_in_cell = carnivores_stays_in_cell

            # Reset possiblity to migrate
        for rows in self.coordinates:
            for cell in rows:
                if cell.herbivore_in_cell:
                    for herbivore in cell.herbivore_in_cell:
                        herbivore.can_migrate = True

                if cell.carnivore_in_cell:
                    for carnivore in cell.carnivore_in_cell:
                        carnivore.can_migrate = True

    def _annual_age_increase(self):
        """
        Every year, every animal in every cell of the map gets a year older
        """
        for rows in self.coordinates:
            for cell in rows:

                if cell.herbivore_in_cell:
                    for herbivore in cell.herbivore_in_cell:
                        herbivore.age_increase()

                if cell.carnivore_in_cell:
                    for carnivore in cell.carnivore_in_cell:
                        carnivore.age_increase()

    def _annual_weight_loss(self):
        """
        Annually decrease weight of all animals
        """
        for rows in self.coordinates:
            for cell in rows:

                if cell.herbivore_in_cell:
                    for herbivore in cell.herbivore_in_cell:
                        herbivore.weight_loss()

                if cell.carnivore_in_cell:
                    for carnivore in cell.carnivore_in_cell:
                        carnivore.weight_loss()

    def _annual_dies_naturally(self):
        """
        If an animal does not die this year it is kept in the list of animals in the cell
        """

        for rows in self.coordinates:
            for cell in rows:

                if cell.herbivore_in_cell:
                    herbivores_alive = []
                    for herbivore in cell.herbivore_in_cell:
                        if not herbivore.dies_naturally():
                            herbivores_alive.append(herbivore)
                    cell.herbivore_in_cell = herbivores_alive

                if cell.carnivore_in_cell:
                    carnivores_alive = []
                    for carnivore in cell.carnivore_in_cell:
                        if not carnivore.dies_naturally():
                            carnivores_alive.append(carnivore)
                    cell.carnivore_in_cell = carnivores_alive

    def annual_cycle(self, num_years):
        """Executes all the annual happenings on Rossumøya
        in the order asked for by project description

        :param num_years: number of years to simulate
        """

        for _ in range(num_years):

            # The annual cycle
            self._annual_regrowth()
            self._annual_eating()
            self._annual_give_birth()
            self._annual_migration()
            self._annual_age_increase()
            self._annual_weight_loss()
            self._annual_dies_naturally()
