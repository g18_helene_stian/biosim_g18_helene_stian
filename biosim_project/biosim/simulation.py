# -*- coding: utf-8 -*-

__author__ = 'Helene Flakke & Stian Teien'

import random
from biosim.map import *
from biosim.visualization import *
import examples.population_generator
from biosim import animals
from biosim.landscape import Jungle, Savannah
import matplotlib.pyplot as plt
import pandas as pd
import subprocess



class BioSim:
    """The simulation.py file will set up the simulation itself and can call upon methods from
    visualization to visualize the results"""
    def __init__(self, island_map, ini_pop=None, seed=123,
                 ymax_animals=None, cmax_animals={'Herbivore': 250, 'Carnivore': 60},
                 img_base=None, img_fmt='png'):
        """
        :param island_map: Multi-line string specifying island geography
        :param ini_pop: List of dictionaries specifying initial population
        :param seed: Integer used as random number seed
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param img_base: String with beginning of file name for figures, including path
        :param img_fmt: String with file type for figures, e.g. 'png'

        If ymax_animals is None, the y-axis limit should be adjusted automatically.

        If cmax_animals is None, sensible, fixed default values should be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,
           {'Herbivore': 50, 'Carnivore': 20}

        If img_base is None, no figures are written to file.
        Filenames are formed as

            '{}_{:05d}.{}'.format(img_base, img_no, img_fmt)

        where img_no are consecutive image numbers starting from 0.
        img_base should contain a path and beginning of a file name.
        """

        random.seed(seed)
        self.kart = Kart(island_map)
        self.kart.insert_animals(ini_pop)
        self._year = 0
        self.graf_list_herb = []
        self.graf_list_carn = []

        self._img_base = img_base
        self._img_fmt = img_fmt
        self._img_ctr = 0

        self.setup_graphics(map=island_map, carn_colorbar_limit=cmax_animals['Carnivore'],
                            herb_colorbar_limit=cmax_animals['Herbivore'],  y_axis_limit=ymax_animals)

    @staticmethod
    def set_animal_parameters(species, params):
        """
        Set parameters for animal species. Will overwrite the default stored in the animal classes.
        Tests first that the parameters are acceptable.

        :param species: String, name of animal species
        :param params: Dict with valid parameter specification for species
        """

        for key, value in params.items():
            if key == "DeltaPhiMax":
                if value == 0:
                    raise ValueError("Parameters out of bounds")
            if key == "eta":
                if value > 1:
                    raise ValueError("Parameters out of bounds")
            if value < 0:
                raise ValueError("Parameters out of bounds")

        if species == 'Herbivore':
            animals.Herbivore.set_parameters(params)

        elif species == 'Carnivore':
            animals.Carnivore.set_parameters(params)

        else:
            raise ValueError

    @staticmethod
    def set_landscape_parameters(landscape, params):
        """
        Set parameters for landscape type. Will overwrite the default stored in landscape.
        Tests first that the parameters are acceptable.

        :param landscape: String, code letter for landscape
        :param params: Dict with valid parameter specification for landscape
        """
        for key, value in params.items():
            if value < 0:
                raise ValueError("Parameters out of bounds")

        if landscape == 'J':
            Jungle.set_parameters(params)

        elif landscape == 'S':
            Savannah.set_parameters(params)

        else:
            raise ValueError

    def simulate(self, num_years, vis_years=1, img_years=None):
        """
        Run simulation and update the visualization

        :param num_years: number of years to simulate
        :param vis_years: years between visualization updates
        :param img_years: years between visualizations saved to files (default: vis_years)
        """

        if img_years is None:
            img_years = vis_years

        self.visualize_map.interrupt_boolean = False
        self.visualize_map.ax19.set_visible(True)  # Shows loading image

        if vis_years > num_years:
            vis_years = num_years

        for _ in range(self._year, self.year+num_years, vis_years):
            self.kart.annual_cycle(vis_years)

            # Visualization
            self.graf_list_herb.append(self.num_animals_per_species['Herbivore'])
            self.graf_list_carn.append(self.num_animals_per_species['Carnivore'])
            self.visualize_map.update_line(self.graf_list_herb, self.graf_list_carn, vis_years)

            self._year += vis_years
            self.visualize_map.update_herb_population()
            self.visualize_map.update_carn_population()
            self.visualize_map.update_animals_alive(self.num_animals_per_species, self._year)

            plt.pause(1e-3)

            # Saves figure
            if img_years:
                if self._year % img_years == 0:
                    self._save_graphics(self._img_base)

            # Interrupt if interrupt-button is clicked
            if self.visualize_map.interrupt_boolean:
                break

        self.visualize_map.ax19.set_visible(False)  # Hides loading image

    def add_population(self, population):
        """
        Add a population to the island

        :param population: List of dictionaries specifying population
        """
        self.kart.insert_animals(population)

    def setup_graphics(self, map, carn_colorbar_limit=60, herb_colorbar_limit=250, y_axis_limit=200):
        """
        Sets up graphics for visual simulation.

        :param map: Multi-line string specifying island geography
        :param carn_colorbar_limit: limit for colour bar in carnivore density map
        :param herb_colorbar_limit: limit for colour bar in herbivore density map
        :param y_axis_limit: y axis limit for population graph
        """
        self.visualize_map = Visualize(map, self.kart.coordinates, self)
        self.visualize_map.setup_graphics(carn_colorbar_limit, herb_colorbar_limit, y_axis_limit)

    @property
    def year(self):
        """The last year that was simulated, so gives total number of years simulated"""
        return self._year

    @property
    def num_animals(self):
        """Total number of animals on island irrespective of position"""
        count_herbivore = 0
        count_carnivore = 0
        for rows in self.kart.coordinates:
            for cell in rows:
                count_herbivore += len(cell.herbivore_in_cell)
                count_carnivore += len(cell.carnivore_in_cell)

        return count_herbivore + count_carnivore

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary."""
        count_herbivore = 0
        count_carnivore = 0
        species_on_island = {}
        for rows in self.kart.coordinates:
            for cell in rows:
                count_herbivore += len(cell.herbivore_in_cell)
                count_carnivore += len(cell.carnivore_in_cell)

        species_on_island["Herbivore"] = count_herbivore
        species_on_island["Carnivore"] = count_carnivore
        return species_on_island

    @property
    def animal_distribution(self):
        """Pandas DataFrame with animal count per species for each cell on island."""

        species_on_island_in_coords = []
        for y, rows in enumerate(self.kart.coordinates):
            for x, cell in enumerate(rows):
                species_on_island_in_coords.append([y+1, x+1, len(cell.herbivore_in_cell),
                                                    len(cell.carnivore_in_cell)])

        df = pd.DataFrame(columns=['Row', 'Col', 'Herbivore', 'Carnivore'],
                          data=species_on_island_in_coords)

        return df

    def _save_graphics(self, _img_base):
        """
        Saves graphics to file, if file name given. Code supplied by H.E. Plesser.
        Image files will be numbered consecutively.
        """

        if _img_base is None:
            return

        plt.savefig('{base}_{num:05d}.{type}'.format(base=_img_base,
                                                     num=self._img_ctr,
                                                     type=self._img_fmt))
        self._img_ctr += 1


    def make_movie(self):
        """Create MPEG4 movie from visualization images saved.

        :param num_years: number of years to simulate
        :param vis_years: years between visualization updates
        :param img_years: years between visualizations saved to files (default: vis_years)

        Creates MPEG4 movie from visualization images saved.
        Code supplied by H.E. Plesser

        .. :note:
            Requires ffmpeg

        The movie is stored as img_base + movie_fmt
        """

        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        try:
            # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
            # section "Compatibility"
            subprocess.check_call(['ffmpeg',
                                   '-i', '{}_%05d.png'.format(self._img_base),
                                   '-y',
                                   '-filter:v', 'setpts=3*PTS',  # movie speed
                                   '-profile:v', 'baseline',
                                   '-level', '3.0',
                                   '-pix_fmt', 'yuv420p',
                                   '{}.{}'.format(self._img_base, "mp4")])
        except subprocess.CalledProcessError as err:
            raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))


if __name__ == '__main__':

    example_map = """\
               OOOOOOOOOOOOOOOOOOOOO
               OJJOOOOOSMMMMJJJJJJJO
               OSJSSSJJJJMMJJJJJJJOO
               OSJSSSSSSSMMJJJJJJOOO
               OSSSSSJJJJJJJJJJJJOOO
               OSSSSSJJJDDJJJSJJJOOO
               OSSJJJJJDDDJJJSSSSOOO
               OOSSSSJJJDDJJJSOOOOOO
               OSSSJJJJJDDJJJJJJJOOO
               OSSSSJJJJDDJJJJOOOOOO
               OOSSSSJJJJJJJJOOOOOOO
               OOOSSSSJJJJJJJOOOOOOO
               OOOOOOOOOOOOOOOOOOOOO"""

    ini_population = examples.population_generator.Population(n_herbivores=3,
                                                              coord_herb=[(2, 2), (3, 2), (4, 2)],
                                                              n_carnivores=0,
                                                              coord_carn=[(2, 2)]).get_animals()

    # - Setup simulation with visualisation -
    sim = BioSim(island_map=example_map, ini_pop=None, seed=123, img_base='../img/')
    # sim.simulate(num_years=100, vis_years=1, img_years=10)

    # - Makes plot interactive and shows plot -
    plt.ion()
    plt.show(block=True)
