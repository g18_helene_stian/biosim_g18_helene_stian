# -*- coding: utf-8 -*-

__author__ = 'Helene Flakke & Stian Teien'


class Landscape:
    def __init__(self):
        """
        landscape.py  contain the various landscape types found on
        Rossumøya and their characteristics
        """
        self.traversible = False
        self.food_available = 0.0
        self.herbivore_in_cell = []
        self.carnivore_in_cell = []


class Jungle(Landscape):
    """"
    A landscape type characterised by immunity to over grassing.
    It is accessible for the animals.
    """
    # f_max = 800.0  # maximum available fodder
    parameters = {'f_max': 800}

    def __init__(self):
        super().__init__()
        self.traversible = True
        self.food_available = self.parameters['f_max']

    # for each year that passes, per cell in map:
    def new_year_new_food(self):
        """method to update the food available for each year"""
        self.food_available = self.parameters['f_max']

    @classmethod
    def set_parameters(cls, para):
        """class method for updating the parameters governing the jungles behavior

        :param para: dictionary of new parameters
        """
        cls.parameters.update(para)


class Savannah(Landscape):
    """
    A landscape type characterised by low resistance to grassing.
    It is accessible for the animals.
    """
    parameters = {'f_max': 300, 'alpha': 0.3}
    # f_max = 300.0  # maximum available fodder
    # alpha = 0.3  # regrowth factor

    def __init__(self):
        super().__init__()
        self.traversible = True
        self.food_available = self.parameters['f_max']

    # for each year that passes, per cell in map:
    def new_year_new_food(self):
        """method to update the food available for each year that passes"""
        self.food_available = (self.parameters['f_max'] - self.food_available)\
            * self.parameters['alpha'] + self.food_available

    @classmethod
    def set_parameters(cls, para):
        """class method for updating the parameters of Savannah

        :param para: dictionary of new parameters
        """
        cls.parameters.update(para)


class Desert(Landscape):
    """
    In the desert there is no food for the herbivores, but they can still move into these cells
    and the carnivores will be able to pray on the herbivores here.
    """

    def __init__(self):
        super().__init__()
        self.traversible = True


class Mountain(Landscape):
    """Mountains inherits not being accessible to any animal type from landscape."""

    def __init__(self):
        super().__init__()


class Ocean(Landscape):
    """Surrounds the island, and is inaccessible to all the animal types."""

    def __init__(self):
        super().__init__()


def give_type(type_la):
    """
    A function to assign type of landscape to the cell in the map

    :param type_la: A letter describing the landscape type
    :return: the landscape type as an object
    """

    give_type_dictionary = {'J': Jungle, 'S': Savannah, 'D': Desert, 'M': Mountain, 'O': Ocean}
    if type_la not in give_type_dictionary:
        raise ValueError
    return give_type_dictionary[type_la]()
