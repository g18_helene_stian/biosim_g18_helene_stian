# -*- coding: utf-8 -*-

__author__ = 'Helene Flakke & Stian Teien'

import pytest
from biosim.map import *




def test_map_created():
    """
    Testing that map exists when called
    """

    example_map_1 = """\
                        OOO
                        OJO
                        OOO"""
    assert Kart(example_map_1)


def test_wrong_landscape():
    """test that wrong imput map gives an error
    """

    example_map_2 = """\
                    OOO
                    OUO
                    OOO"""
    with pytest.raises(ValueError):
        Kart(example_map_2)


def test_ocean_endges():
    """test map always have Ocean cells as outer limit, aka its an island
    """

    example_map_3 = """\
                    OOOO
                    OJJJ
                    OOOO"""
    example_map_4 = """\
                        OJJO
                        OOOO
                        OOOO"""
    with pytest.raises(ValueError):
        Kart(example_map_3)
        Kart(example_map_4)


def test_animals_added_in_cell():
    """test animals gets added to the cells"""
    example_map_5 = """\
                           OOO
                           OJO
                           OOO"""
    kart = Kart(example_map_5)
    pre_pop = len(kart.coordinates[1][1].herbivore_in_cell + kart.coordinates[1][1].carnivore_in_cell)
    animals = [{'loc': (2, 2),
      'pop': [{'species': 'Herbivore', 'age': 10, 'weight': 15},
              {'species': 'Herbivore', 'age': 5, 'weight': 40},
              {'species': 'Herbivore', 'age': 15, 'weight': 25}]}]
    kart.insert_animals(animals)
    post_pop = len(kart.coordinates[1][1].herbivore_in_cell + kart.coordinates[1][1].carnivore_in_cell)
    assert pre_pop < post_pop


def right_amount_of_animals():
    """teste riktig mengde dyr per celle"""
    example_map_5 = """\
                               OOO
                               OJO
                               OOO"""
    kart = Kart(example_map_5)

    animals = [{'loc': (2, 2),
                'pop': [{'species': 'Herbivore', 'age': 10, 'weight': 15},
                        {'species': 'Herbivore', 'age': 5, 'weight': 40},
                        {'species': 'Herbivore', 'age': 15, 'weight': 25}]}]
    kart.insert_animals(animals)
    animals_in_this_cell = len(kart.coordinates[1][1].herbivore_in_cell + kart.coordinates[1][1].carnivore_in_cell)
    assert animals_in_this_cell == len(animals[0]['pop'])


def test_unrecognized_species():
    """teste feilmelding for feil art"""
    example_map_6 = """\
                               OOO
                               OJO
                               OOO"""
    kart = Kart(example_map_6)
    animals = [{'loc': (2, 2),
                'pop': [{'species': 'Bird', 'age': 10, 'weight': 15}]}]
    with pytest.raises(ValueError):
        kart.insert_animals(animals)


    """teste riktig mengde dyr per celle"""


def test_zero_or_more_animals_in_cells():
    """sjekke at mengde dyr alltid 0 eller positiv - inkludert sletting av dyr i celler  - senere"""
    geografi = """\
                   OOOOOOOOO
                   OJJJJJJJO
                   OOOOOOOOO"""
    kart = Kart(geografi)
    animals =     [{'loc': (2, 2),
      'pop': [{'species': 'Herbivore', 'age': 10, 'weight': 15},
              {'species': 'Herbivore', 'age': 5, 'weight': 40},
              {'species': 'Herbivore', 'age': 15, 'weight': 25}]},
     {'loc': (2, 3),
      'pop': [{'species': 'Herbivore', 'age': 2, 'weight': 60},
              {'species': 'Herbivore', 'age': 9, 'weight': 30},
              {'species': 'Herbivore', 'age': 16, 'weight': 14}]},
     {'loc': (2, 5),
      'pop': [{'species': 'Carnivore', 'age': 3, 'weight': 35},
              {'species': 'Carnivore', 'age': 5, 'weight': 20},
              {'species': 'Carnivore', 'age': 8, 'weight': 5}]}]

    kart.insert_animals(animals)
    for rows in kart.coordinates:
        for cell in rows:
            assert len(cell.herbivore_in_cell + cell.carnivore_in_cell) >= 0


def test_annual_methods_exist():
    """test that the Kart class does contain the methods"""
    assert Kart._annual_regrowth
    assert Kart._annual_eating
    assert Kart._annual_give_birth
    assert Kart._annual_migration
    assert Kart._annual_age_increase
    assert Kart._annual_weight_loss
    assert Kart._annual_dies_naturally



