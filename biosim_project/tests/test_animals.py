# -*- coding: utf-8 -*-

import pytest
from biosim.animals import Animal, Herbivore, Carnivore

"""
Tests for the animals on the island
"""


"""lag test over fødselsvekt tapt fra mor"""
class Test_Herbivore:
    """A testing class for everything related to the herbivore"""

    w_birth = 8  # fødselsvektmean
    sigma_birth = 1.5  # fødselsvektavvik
    beta = 0.9  # vekt_auke_faktor
    eta = 0.05  # vekt:taps_faktor

    a_half = 40.0  # alder_fitness_konstant
    phi_age = 0.2  # age factor?
    w_half = 10.0  # vekt-fitness-konstant
    phi_weight = 0.1  # vekt-faktor

    mu = 0.25  # migration faktor
    gamma = 0.2  # give-birth-faktor
    zeta = 3.5  # second-birth-faktor
    xi = 1.2  # lost-birth weight-multiplier
    omega = 0.4  # birth weight threshold mother
    F = 10.00  # amount of food wanted



    def test_birthweight_lost(self):
        """
        test that the herbivore loses weight when giving birth
        """
        sau = Herbivore(age=4, weight=200)
        pre_weight = sau.weight
        sau.give_birth(N=10)
        post_weight = sau.weight
        assert pre_weight > post_weight


    """vil teste fitness innenfor 0-1"""
    def test_calc_fitness(self):
        """Asserts that the fitness of the herivore never goes below 0 or above one.
        Is perhaps a bit time/memory-heavy
        """

        for age in range(1, 500):
            for weight in range(1, 500):
                sau = Herbivore(age, weight)
                fitness = sau._calc_fitness()
                assert fitness >= 0 and fitness <= 1


    """teste age og weight aldri under 0"""
    def test_age_and_weight_not_under_zero(self):
        """"
        Test herbivore's age and weight are not under zero
        """
        with pytest.raises(ValueError):
            sau_under_age = Herbivore(age=-10, weight=10)
            sau_under_weight = Herbivore(age=10, weight=-10)


    """at alt som skal vere 0-! sannsyn er det, t.d. migration"""
    # trenger migrate til å fungere/existere - kanskje skrive om seinere
    """   def test_migrate_probability(self):
        
        Test herbivore's probability to migrate when it's 100%
        
        sau = Herbivore(age=10, weight=10)
        assert sau.migrate(fitness=1, mu=1) == True"""


    """test weight increases if animals eats"""
    def test_gets_to_eat(self):
        """
        Test if herbivore gain weight when eating
        """
        food_available = 25
        sau = Herbivore(age=10, weight=10)
        pre_eat_weigth = sau.weight
        sau.eat(food_available)
        post_eat_weigth = sau.weight
        assert pre_eat_weigth < post_eat_weigth

