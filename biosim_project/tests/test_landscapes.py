# -*- coding: utf-8 -*-

__author__ = 'Helene Flakke & Stian Teien'

import pytest
from biosim.landscape import *
import inspect

def test_exists():
    """testing if jungle exist"""
    assert Jungle
    assert Savannah
    assert Mountain


def test_is_a_class():
    """test that the classes are classes"""
    assert inspect.isclass(Jungle)
    assert inspect.isclass(Landscape)
    assert inspect.isclass(Mountain)

def test_traversible():
    """test that the correct landscapes are traversible"""
    assert Jungle().traversible
    assert Savannah().traversible
    assert Desert().traversible


def test_not_traversible():
    """test that there is no transversing the uninhibitable areas"""
    assert not Landscape().traversible
    assert not Mountain().traversible
    assert not Ocean().traversible

def test_has_animal_lists():
    """test that the list over animals exists"""
    celle = give_type('J')
    assert isinstance(celle.herbivore_in_cell, list)
    assert isinstance(celle.carnivore_in_cell, list)


def test_illegal_landscape_type():
    """test that wrong input in key assignment for map raises errors"""
    with pytest.raises(ValueError):
        give_type('X')

