.. BioLab_G18 documentation master file, created by
   sphinx-quickstart on Thu Jan 10 14:02:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BioLab_G18's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   animal
   landscape
   map
   simulation
   visualization
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

