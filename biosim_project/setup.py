# -*- coding: utf-8 -*-

from setuptools import setup
import codecs
import os

def read_readme():
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
        long_description = f.read()
    return long_description


setup(
      # Basic information
      name='BioSim',
      version='0.1.0',

      # Packages to include
      packages=['biosim'],

      # Required packages not included in Python standard library
      requires=['pytest','ffmpeg'],

      # Metadata
      description='BioLab Simulation',
      long_description=read_readme(),
      author='Stian Teien, Helene Flakke, NMBU',
      author_email='helenfla@nmbu.no',
      url='https://bitbucket.org/g18_helene_stian/biosim_g18_helene_stian',
      keywords='simple ecological simulation of island',
      license='MIT License',
      classifiers=[
        'Development Status :: Beta',
        'Intended Audience :: Developers',
        'Topic :: Science :: Stochastic processes',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        ]
)

